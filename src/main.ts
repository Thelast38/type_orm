import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as express from "express";
import * as path from "path";
import * as bodyParser from "body-parser";
import "reflect-metadata";
async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(express.static(path.join(__dirname, "public")));
  app.use(bodyParser.urlencoded({extended:true}));
  await app.listen(3000);
 
}
bootstrap();
