import { Test, TestingModule } from '@nestjs/testing';
import { CreatepdfController } from './createpdf.controller';

describe('Createpdf Controller', () => {
  let controller: CreatepdfController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CreatepdfController],
    }).compile();

    controller = module.get<CreatepdfController>(CreatepdfController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
