import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CreatepdfController } from './createpdf/createpdf.controller';

@Module({
  imports: [],
  controllers: [AppController, CreatepdfController],
  providers: [AppService],
})
export class AppModule {}
